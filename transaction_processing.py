import pandas as pd
import numpy as np
import bisect
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score, recall_score
from sklearn import preprocessing

fraud_data = pd.read_csv('./Fraud-Data/Fraud-Data/Fraud_Data.csv')
countries = pd.read_csv('./Fraud-Data/Fraud-Data/IpAddress_to_Country.csv')

def find_country(ip_address, axis):
    position = bisect.bisect(countries['lower_bound_ip_address'], ip_address)
    if position >= countries.shape[0]:
        position = position - 1
    print countries.loc[[position]]['country']
    return countries.iloc[position]['country']


# Create csv file after get country from ip address
fraud_data['country'] = fraud_data['ip_address'].apply(find_country, axis=1)
fraud_data.to_csv('./Fraud-Data/Fraud-Data/Fraud_Data_Country.csv')

# Load data with country field
fraud_data_country = pd.read_csv('./Fraud-Data/Fraud-Data/Fraud_Data_Country.csv')

# Convert time object to timestamp
fraud_data_country['signup_time'] = fraud_data_country['signup_time'].apply(pd.to_datetime).apply(lambda time: (time - np.datetime64('1970-01-01T00:00:00Z'))/np.timedelta64(1, 's'))
fraud_data_country['purchase_time']= fraud_data_country['purchase_time'].apply(pd.to_datetime).apply(lambda time: (time - np.datetime64('1970-01-01T00:00:00Z'))/np.timedelta64(1, 's'))

print fraud_data_country.head()
print fraud_data_country.info()
print fraud_data_country.describe()
print fraud_data_country['device_id'].nunique()
# Have 137956 device ids. num of devices/ num of rows = 137956/151112 = 0.912 => drop device_id

# Get different time between purchase_time and sigup_time
fraud_data_country['subtract_time'] = fraud_data_country['purchase_time'] - fraud_data_country['signup_time']
fraud_data_country = fraud_data_country.drop(['user_id', 'purchase_time', 'signup_time', 'device_id', 'ip_address'], axis=1)

# Convert string label to numeric label
numeric_label = preprocessing.LabelEncoder()

fraud_data_country['sex'] = numeric_label.fit_transform(fraud_data_country['sex'])
fraud_data_country['source'] = numeric_label.fit_transform(fraud_data_country['source'])
fraud_data_country['country'] = numeric_label.fit_transform(fraud_data_country['country'])
fraud_data_country['browser'] = numeric_label.fit_transform(fraud_data_country['browser'])


print fraud_data_country.head()

x_data = fraud_data_country.drop(['class'], axis=1)
y_data = fraud_data_country['class']

# Split data to 2 chunks with proportion is 8:2, 0.8 for train, 0.2 for test
x_data_train, x_data_test, y_data_train, y_data_test = train_test_split(x_data, y_data, test_size=0.20)

# Create model RandomForest
model = RandomForestClassifier(n_estimators=50)
model.fit(x_data_train, y_data_train)

predictive_data = model.predict(x_data_test)

# Calculate precision = true-positive/(true-positive + false-positive), recall = true-positive/(true-positive + false-negative)
print precision_score(y_data_test, predictive_data)
print recall_score(y_data_test, predictive_data)



